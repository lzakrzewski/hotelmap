<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Reference;
use HotelMap\Console\RoomsListCommand;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Client;
use HotelMap\Adapter\ApiAdapter;
use HotelMap\Adapter\GuzzleApiAdapter;
use HotelMap\Adapter\RoomDataExtractor;

$container
    ->register(ClientInterface::class, Client::class)
    ->addArgument(['base_uri' => 'https://apis.ihg.com']);

$container
    ->register(GuzzleApiAdapter::class, GuzzleApiAdapter::class)
    ->addArgument(new Reference(ClientInterface::class));

$container
    ->register(ApiAdapter::class, RoomDataExtractor::class)
    ->addArgument(new Reference(GuzzleApiAdapter::class));

$container
    ->register(RoomsListCommand::class, RoomsListCommand::class)
    ->setPublic(true)
    ->addTag('console')
    ->addArgument(new Reference(ApiAdapter::class));

