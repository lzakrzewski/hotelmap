## Installation
`composer install`    

## Usage
`bin/console rooms-list "2019-04-20" "2019-09-22" 2`

- 1st argument - start date,    
- 2nd argument - end date,   
- 3rd argument - number of adults.  

## Testing
`composer tests-all`
