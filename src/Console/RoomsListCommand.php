<?php

declare(strict_types=1);

namespace HotelMap\Console;

use HotelMap\Adapter\ApiAdapter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RoomsListCommand extends Command
{
    /** @var ApiAdapter */
    private $apiAdapter;

    public function __construct(ApiAdapter $apiAdapter)
    {
        parent::__construct('rooms-list');

        $this->apiAdapter = $apiAdapter;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('start', InputArgument::REQUIRED)
            ->addArgument('end', InputArgument::REQUIRED)
            ->addArgument('adults', InputArgument::REQUIRED)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln(
            json_encode(
                $this->apiAdapter->get(new \DateTime('2019-04-01'), new \DateTime('2019-04-20'), 2),
                JSON_PRETTY_PRINT
            )
        );
    }
}
