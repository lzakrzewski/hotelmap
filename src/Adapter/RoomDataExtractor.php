<?php

declare(strict_types=1);

namespace HotelMap\Adapter;

class RoomDataExtractor implements ApiAdapter
{
    /** @var ApiAdapter */
    private $apiAdapter;

    public function __construct(ApiAdapter $apiAdapter)
    {
        $this->apiAdapter = $apiAdapter;
    }

    public function get(\DateTimeInterface $start, \DateTimeInterface $end, int $numberOfAdults): array
    {
        $data = $this->apiAdapter->get($start, $end, $numberOfAdults);

        if (false === isset($data['rooms']) || false === isset($data['rates'])) {
            throw new \InvalidArgumentException('Invalid data');
        }

        $roomsData = $data['rooms'];
        $ratesData = $data['rates'];

        return array_values(
            array_map(
                function (array $roomData) use ($ratesData) {
                    $rateData = $ratesData[$roomData['rateCode']];

                    return [
                        'RoomName'          => $roomData['description'],
                        'CurrencyCode'      => $rateData['currencyCode'],
                        'NightlyRate'       => $roomData['charges']['avgNightlyRate'],
                        'TotalBeforeTax'    => $roomData['charges']['priceTotal'],
                        'TotalAfterTax'     => $roomData['charges']['total'],
                        'CancellationTerms' => trim($rateData['cancellationPolicy']),
                    ];
                },
                $roomsData
        ));
    }
}
