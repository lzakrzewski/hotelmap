<?php

declare(strict_types=1);

namespace HotelMap\Adapter;

interface ApiAdapter
{
    /**
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @param int                $numberOfAdults
     *
     * @return array
     */
    public function get(\DateTimeInterface $start, \DateTimeInterface $end, int $numberOfAdults): array;
}
