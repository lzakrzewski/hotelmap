<?php

declare(strict_types=1);

namespace HotelMap\Adapter;

use GuzzleHttp\ClientInterface;

class GuzzleApiAdapter implements ApiAdapter
{
    /** @var ClientInterface */
    private $guzzle;

    public function __construct(ClientInterface $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    public function get(\DateTimeInterface $start, \DateTimeInterface $end, int $numberOfAdults): array
    {
        $response = $this->guzzle->request(
            'POST',
            'guest-api/v1/ihg/us/en/rates',
            [
                'body'           => $this->getBody($start, $end, $numberOfAdults),
                'headers'        => [
                    'content-type'        => 'application/json; charset=UTF-8',
                    'x-ihg-api-key'       => 'se9ym5iAzaW8pxfBjkmgbuGjJcr3Pj6Y',
                    'x-ihg-mws-api-token' => '58ce5a89-485a-40c8-abf4-cb70dba4229b',
                ],
            ]
        );

        return (array) json_decode($response->getBody()->getContents(), true);
    }

    private function getBody(\DateTimeInterface $start, \DateTimeInterface $end, int $numberOfAdults): string
    {
        return json_encode([
            'hotelCode'      => 'ADLAH',
            'adults'         => $numberOfAdults,
            'children'       => 0,
            'rateCode'       => '6CBARC',
            'showPointsRate' => true,
            'rooms'          => 1,
            'version'        => '1.2',
            'corporateId'    => '',
            'travelAgencyId' => '99801505',
            'dateRange'      => [
                'start' => $start->format('Y-m-d'),
                'end'   => $end->format('Y-m-d'),
            ],
            'memberID' => null,
        ]);
    }
}
