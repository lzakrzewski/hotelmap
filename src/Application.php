<?php

declare(strict_types=1);

namespace HotelMap;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\DependencyInjection\TaggedContainerInterface;

class Application extends ConsoleApplication
{
    /** @var TaggedContainerInterface */
    private $container;

    public function __construct()
    {
        parent::__construct('hotel-map', '0');

        $this->container = new ContainerBuilder();
        $this->setUpContainer();
    }

    private function setUpContainer(): void
    {
        $loader = new PhpFileLoader($this->container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.php');

        foreach ($this->container->findTaggedServiceIds('console') as $commandId => $command) {
            $this->add($this->container->get($commandId));
        }
    }

    public function getContainer(): TaggedContainerInterface
    {
        return $this->container;
    }
}
