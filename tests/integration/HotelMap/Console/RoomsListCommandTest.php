<?php

declare(strict_types=1);

namespace tests\integration\HotelMap\Console;

use HotelMap\Application;
use HotelMap\Console\RoomsListCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;

class RoomsListCommandTest extends TestCase
{
    /** @var CommandTester */
    private $tester;

    /** @test */
    public function it_can_return_data_from_api(): void
    {
        $this->executeConsoleCommand(
            [
                'start'  => '2019-04-01',
                'end'    => '2019-04-20',
                'adults' => 2,
            ]
        );

        $this->assertEquals(0, $this->tester->getStatusCode());
    }

    private function executeConsoleCommand(array $arguments = []): void
    {
        $this->tester->execute($arguments);
    }

    protected function setUp(): void
    {
        $cli          = (new Application())->getContainer()->get(RoomsListCommand::class);
        $this->tester = new CommandTester($cli);
    }
}
