<?php

declare(strict_types=1);

namespace tests\unit\HotelMap\Adapter;

use HotelMap\Adapter\ApiAdapter;
use HotelMap\Adapter\RoomDataExtractor;
use PHPUnit\Framework\TestCase;

class RoomDataExtractorTest extends TestCase
{
    /** @var ApiAdapter */
    private $apiAdapter;

    /** @var RoomDataExtractor */
    private $extractor;

    /** @test */
    public function it_can_extract_data_from_api(): void
    {
        $data = $this->extractor->get(new \DateTime('2019-01-01'), new \DateTime('2019-01-31'), 2);

        $this->assertCount(26, $data);
        $lastResult = end($data);
        $this->assertEquals(
            [
                'RoomName'          => 'STANDARD ROOM',
                'CurrencyCode'      => 'AUD',
                'NightlyRate'       => 205.58,
                'TotalBeforeTax'    => 3906,
                'TotalAfterTax'     => 3906,
                'CancellationTerms' => 'Canceling your reservation before 6:00 PM (local hotel time) on Sunday, 31 March, 2019 will result in no charge. Canceling your reservation after 6:00 PM (local hotel time) on 31 March, 2019, or failing to show, will result in a charge equal to the first night\'s stay per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.',
            ],
            $lastResult
        );
    }

    /** @test */
    public function it_fails_when_data_is_invalid(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->apiAdapter->get(new \DateTime('2019-01-01'), new \DateTime('2019-01-31'), 2)
            ->willReturn(['invalid']);

        $this->extractor->get(new \DateTime('2019-01-01'), new \DateTime('2019-01-31'), 2);
    }

    protected function setUp(): void
    {
        $response = (array) json_decode(file_get_contents(__DIR__.'/fixtures/api_data.json'), true);

        $this->apiAdapter = $this->prophesize(ApiAdapter::class);
        $this->apiAdapter->get(new \DateTime('2019-01-01'), new \DateTime('2019-01-31'), 2)
            ->willReturn($response);

        $this->extractor = new RoomDataExtractor($this->apiAdapter->reveal());
    }
}
